package tech.coolmathgames.spiggy.patcher;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;

import androidx.annotation.WorkerThread;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

public class StorageController {
  private final SpiggyApp spiggyApp;
  private final DownloadManager downloadManager;
  private final SharedPreferences prefs;
  private final Gson gson;
  private List<PatchedApp> apps;

  public StorageController(SpiggyApp spiggyApp) {
    this.spiggyApp = spiggyApp;
    this.prefs = spiggyApp.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    GsonBuilder builder = new GsonBuilder();
    this.gson = builder.create();
    this.downloadManager = (DownloadManager) spiggyApp.getSystemService(Context.DOWNLOAD_SERVICE);
  }

  public PatchedApp createApp(String displayName, String packageName, String branch, boolean hasIcon) {
    PatchedAppData data = new PatchedAppData(displayName, packageName, branch, hasIcon);
    PatchedApp app = new PatchedApp(data, this);
    this.getApps().add(app);
    this.syncApps();
    return app;
  }

  @WorkerThread
  public List<PatchedApp> getApps() {
    if (this.apps == null) {
      System.out.println("Parsing app list, this is slow!");
      String apps = this.prefs.getString("apps", "[]");
      Type listType = new TypeToken<List<PatchedAppData>>() {
      }.getType();
      List<PatchedAppData> appsList = gson.fromJson(apps, listType);
      this.apps = new ArrayList<PatchedApp>();
      for (PatchedAppData data : appsList) {
        PatchedApp patchedApp = new PatchedApp(data, this);
        this.apps.add(patchedApp);
      }
    }
    return this.apps;
  }

  public File getIconFolder() {
    File dataDir = this.getSpiggyApp().getFilesDir();
    return new File(dataDir, "icons");
  }

  @WorkerThread
  public void syncApps() {
    SharedPreferences.Editor editor = this.prefs.edit();
    List<PatchedAppData> appDataList = new ArrayList<PatchedAppData>();
    for (PatchedApp app : apps) {
      appDataList.add(app.getData());
    }
    editor.putString("apps", gson.toJson(appDataList));
    editor.commit();
    Objects.requireNonNull(this.spiggyApp.getActivity()).syncApps();
  }

  public SpiggyApp getSpiggyApp() {
    return this.spiggyApp;
  }

  // Okay, now this is epic: https://stackoverflow.com/a/27869798
  public KeyStore.PrivateKeyEntry getSigningKey()
      throws NoSuchProviderException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
      IOException, InvalidAlgorithmParameterException, UnrecoverableEntryException {
    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
    keyStore.load(null);

    String alias = "signingKey"; // replace as required or get it as a function argument

    // Create the keys if necessary
    if (!keyStore.containsAlias(alias)) {
      Calendar notBefore = Calendar.getInstance();
      Calendar notAfter = Calendar.getInstance();
      notAfter.add(Calendar.YEAR, 100);
      String algorithm;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        algorithm = KeyProperties.KEY_ALGORITHM_RSA;
      } else {
        algorithm = "RSA";
      }
      KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(this.spiggyApp)
          .setAlias(alias)
          .setKeyType(algorithm)
          .setKeySize(4096)
          .setSubject(new X500Principal("CN=tech.coolmathgames.spiggy"))
          .setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()))
          .setStartDate(notBefore.getTime())
          .setEndDate(notAfter.getTime())
          .build();
      KeyPairGenerator generator = KeyPairGenerator.getInstance(algorithm, "AndroidKeyStore");
      generator.initialize(spec);

      KeyPair keyPair = generator.generateKeyPair();
    }

    // Retrieve the keys
    return (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
  }

  public void updateBranches() {
    Set<String> branches = new HashSet<String>();
    for(PatchedApp app : this.getApps()) {
      branches.add(app.getBranch());
    }
    // We don't *really* care about the result of this
    for(String branch : branches) {
      updateBranch(branch);
    }
  }

  public void updateBranch(String branch) {
    Uri uri = Uri.parse("https://gitlab.com/api/v4/projects/" + SpiggyApp.PROJECT_NAME + "/jobs/artifacts/" + branch + "/raw/dist/preload.js?job=preload");
    DownloadManager.Request request = new DownloadManager.Request(uri)
        .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
            DownloadManager.Request.NETWORK_MOBILE)
        .setAllowedOverRoaming(true)
        .setAllowedOverMetered(true)
        .setTitle("Preload for " + branch)
        .setDescription("Downloading preload for " + branch)
        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "spiggy/" + branch + ".js");
    this.downloadManager.enqueue(request);
  }
}
