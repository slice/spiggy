package tech.coolmathgames.spiggy.patcher;

public abstract interface RefreshableFragment {
  public void refreshApps();
}
