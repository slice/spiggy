use directories::ProjectDirs;
use lzma_rs;
use reqwest::blocking::Client;
use reqwest::header::ACCEPT;
use reqwest::StatusCode;
use serde::Deserialize;
use std::error::Error;
use std::fs::{File, OpenOptions};
use std::io::{BufReader, Seek, SeekFrom, Write};
use std::path::Path;
use std::{fmt, fs};

#[derive(Deserialize, Clone)]
struct Release {
  assets: Vec<Asset>,
  id: usize,
}
#[derive(Deserialize, Clone)]
struct Asset {
  name: String,
  // size: usize,
  url: String,
}

#[derive(Debug)]
pub enum GadgetError {
  UnknownStatusCode(StatusCode, String),
  MissingArchitecture,
}

impl std::error::Error for GadgetError {}

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

impl fmt::Display for GadgetError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    return match self {
      GadgetError::UnknownStatusCode(status, text) => f.write_fmt(format_args!(
        "Unknown status code: {:?}. Text: {}",
        status, text
      )),
      GadgetError::MissingArchitecture => f.write_str("Unsupported Architecture"),
    };
  }
}

pub struct Gadget {
  client: Client,
  cached_release: Option<Option<Release>>,
}

fn get_cache_dir() -> String {
  match cfg!(target_os = "android") {
    false => ProjectDirs::from("tech", "coolmathgames", "spiggy")
      .unwrap()
      .cache_dir()
      .to_str()
      .unwrap()
      .to_string(),
    true => "/sdcard/Download/spiggy".to_string(),
  }
}

impl Gadget {
  pub fn new() -> Gadget {
    let client: Client = match Client::builder().user_agent(APP_USER_AGENT).build() {
      Ok(res) => res,
      Err(err) => {
        error!("Couldn't initialize client: {:?}", err);
        panic!("{:?}", err);
      }
    };
    Gadget {
      client,
      cached_release: None,
    }
  }

  pub fn pull_gadget(&mut self, architecture: &str) -> Result<File, Box<dyn Error + Send + Sync>> {
    let cache_dir = get_cache_dir();
    let architecture = {
      if architecture.starts_with("armeabi") {
        "arm"
      } else if architecture.starts_with("arm64") {
        "arm64"
      } else if architecture == "x86_64" {
        "x86_64"
      } else if architecture == "x86" {
        "x86"
      } else {
        return Err(Box::new(GadgetError::MissingArchitecture));
      }
    };
    let suffix = format!("-android-{}.so.xz", architecture);
    debug!("Pulling gadget");

    let release_cache = match &self.cached_release {
      Some(Some(response)) => Some(response.clone()),
      Some(None) => None,
      None => {
        self.cached_release = Some(None);
        let release: Option<Release> = match self
          .client
          .get("https://api.github.com/repos/frida/frida/releases/latest")
          .send()
        {
          Ok(response) => match response.json::<Release>() {
            Ok(value) => {
              self.cached_release = Some(Some(value.clone()));
              Some(value)
            }
            Err(err) => {
              error!("Couldn't get release: {:?}", err);
              None
            }
          },
          Err(err) => {
            error!("Couldn't get release: {:?}", err);
            None
          }
        };
        release
      }
    };

    let reader: File = match release_cache {
      Some(release) => match self.pull_gadget_cached(architecture, Some(release.id)) {
        Ok(reader) => reader,
        Err(_) => {
          debug!("Gadget wasn't cached. Pulling from github release.");
          let response = self
            .client
            .get(
              release
                .assets
                .into_iter()
                .find(|asset| {
                  asset.name.starts_with("frida-gadget-") && asset.name.ends_with(&suffix)
                })
                .ok_or("No release for architecture")?
                .url,
            )
            .header(ACCEPT, "application/octet-stream")
            .send()?;
          match response.status() {
            StatusCode::OK => {
              let gadget_dir = Path::new(&cache_dir).join("gadget");
              let release_dir = gadget_dir.join(release.id.to_string());

              fs::create_dir_all(release_dir.clone())?;
              let mut file = OpenOptions::new()
                .write(true)
                .truncate(true)
                .read(true)
                .create(true)
                .open(release_dir.join(architecture.to_owned() + ".so"))?;
              let mut response_buf = BufReader::new(response);
              lzma_rs::xz_decompress(&mut response_buf, &mut file)?;
              file.seek(SeekFrom::Start(0))?;

              let mut version_file = File::create(gadget_dir.join("latest.txt"))?;
              version_file.write_all(release.id.to_string().as_bytes())?;

              file
            }
            status => {
              return Err(Box::new(GadgetError::UnknownStatusCode(
                status,
                response.text()?,
              )))
            }
          }
        }
      },
      None => self.pull_gadget_cached(architecture, None)?,
    };
    Ok(reader)
  }
  pub fn pull_gadget_cached(
    &self,
    architecture: &str,
    version: Option<usize>,
  ) -> Result<File, Box<dyn Error + Send + Sync>> {
    let cache_dir = get_cache_dir();
    let version = match version {
      Some(version) => version,
      None => String::from_utf8_lossy(&fs::read(
        Path::new(&cache_dir).join("gadget").join("latest.txt"),
      )?)
      .parse::<usize>()?,
    };
    debug!("Attempting cached gadget {}", version);
    let path = Path::new(&cache_dir)
      .join("gadget")
      .join(version.to_string())
      .join(architecture.to_owned() + ".so");
    let file = File::open(path.clone())?;
    debug!("Pulled cached gadget: {:?}", path);
    Ok(file)
  }
}
