#include "lib.hpp"

LIEF::ELF::Binary* LIEF_ELF_Parser_parse(const uint8_t* data, const size_t dataLength, const char* filename, int mode) {
  std::vector<uint8_t> zipData(dataLength);
  zipData.assign(data, data + dataLength);
  std::string name(filename);
  return LIEF::ELF::Parser::parse(zipData, name, (LIEF::ELF::DYNSYM_COUNT_METHODS)mode).release();
}

void LIEF_ELF_Binary_add_library(LIEF::ELF::Binary* binary, const char* filename) {
  std::string name(filename);
  binary->add_library(name);
}

uint8_t* LIEF_ELF_Binary_raw(LIEF::ELF::Binary* binary, size_t* length) {
  std::vector<uint8_t> data = binary->raw();
  *length = data.size();
  uint8_t* rawData = (uint8_t*)malloc(data.size());
  memcpy(rawData, data.data(), data.size());
  return rawData;
}

void LIEF_free(void* ptr) {
  free(ptr);
}
void LIEF_ELF_Binary_delete(LIEF::ELF::Binary* binary) {
  delete binary;
}
