module.exports = {
  init: () => {
    // Viewer:
    Java.use("java.util.regex.Pattern").compile.overload(
      "java.lang.String"
    ).implementation = function (pattern) {
      if (pattern == "^<(a)?:([a-zA-Z_0-9]+):(\\d+)>") {
        pattern = "^<(?:\u200b|&)?(a)?:([a-zA-Z_0-9]+):(\\d+)>";
      }
      if (pattern == "<(a)?:([a-zA-Z_0-9]+):(\\d+)>") {
        pattern = "<(?:\u200b|&)?(a)?:([a-zA-Z_0-9]+):(\\d+)>";
      }
      return this.compile(pattern);
    };
    const Rules = Java.use("com.discord.utilities.textprocessing.Rules");
    Rules.REGEX_CUSTOM_EMOJI.value = "<&?(a)?:([a-zA-Z_0-9]+):(\\d+)>";

    // Sender:
    const ModelEmojiCustom = Java.use(
      "com.discord.models.domain.emoji.ModelEmojiCustom"
    );

    ModelEmojiCustom.getMessageContentReplacement.implementation = function () {
      const normalContent = this.getMessageContentReplacement();

      if (isMePremiumReal) {
        return normalContent;
      } else {
        return normalContent[0] + "\u200b" + normalContent.slice(1);
      }
    };
    // May not be needed
    ModelEmojiCustom.isUsable.implementation = function () {
      return true;
    };

    const buildUsableEmojiSet = Java.use(
      "com.discord.stores.StoreEmoji$buildUsableEmojiSet$5"
    );
    let isMePremiumReal = null;
    buildUsableEmojiSet.$init.implementation = function (
      allCustomEmojis,
      isExternalEmoji,
      isExternalEmojiRestricted,
      includeUnavailableEmojis,
      emojiContext,
      isMePremium,
      includeUnusableEmojis,
      emojiNameCounts,
      emojiIdsMap,
      usableCustomEmojis
    ) {
      isMePremiumReal = isMePremium;
      isMePremium = true;
      return this.$init(
        allCustomEmojis,
        isExternalEmoji,
        isExternalEmojiRestricted,
        includeUnavailableEmojis,
        emojiContext,
        isMePremium,
        includeUnusableEmojis,
        emojiNameCounts,
        emojiIdsMap,
        usableCustomEmojis
      );
    };
  },
};
