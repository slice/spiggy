module.exports = {
  init: () => {
    Java.perform(function () {
      const WidgetChatListAdapterItemMessage = Java.use(
        "com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemMessage"
      );
      const MessageEntry = Java.use(
        "com.discord.widgets.chat.list.entries.MessageEntry"
      );
      const Long = Java.use("java.lang.Long");
      const CoreUser = Java.use("com.discord.models.user.CoreUser");
      const UserUtils = Java.use("com.discord.utilities.user.UserUtils");
      // const WidgetChatListAdapter = Java.use(
      //   "com.discord.widgets.chat.list.adapter.WidgetChatListAdapter"
      // );
      const JString = Java.use("java.lang.String");

      WidgetChatListAdapterItemMessage.onConfigure.overload(
        "int",
        "com.discord.widgets.chat.list.entries.ChatListEntry"
      ).implementation = function (index, chatListEntry) {
        try {
          this.onConfigure(index, chatListEntry);

          if (this.itemName.value != null) {
            const messageEntry = Java.cast(chatListEntry, MessageEntry);
            const message = messageEntry.getMessage();
            const nickOrUsernames = messageEntry.getNickOrUsernames();
            const author = message.getAuthor();
            const authorAsUser = CoreUser.$new(author);

            const displayName = nickOrUsernames
              .get(author != null ? Long.valueOf(author.i().toString()) : null)
              .toString();
            const discrim =
              UserUtils.INSTANCE.value.getDiscriminatorWithPadding(
                authorAsUser
              );

            const name =
              displayName == authorAsUser.getUsername()
                ? displayName + discrim
                : `${displayName} (${authorAsUser.getUsername()}${discrim})`;

            this.itemName.value.setText(JString.$new(name));
          }
        } catch (err) {
          console.error("[showTag]", err.stack);
        }
      };
    });
  },
};
