module.exports = {
  init: () => {
    Java.perform(function () {
      const WidgetChatListAdapterItemMessage = Java.use(
        "com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemMessage"
      );
      const onConfigure = Java.use(
        "com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemMessage$onConfigure$5"
      );

      onConfigure.onClick.implementation = function () {
        const adapter = WidgetChatListAdapterItemMessage.access$getAdapter$p(
          this.this$0.value
        );
        adapter.eventHandler.value.onMessageAuthorNameClicked(
          this.$message.value,
          adapter.getData().getGuildId()
        );
      };
    });
  },
};
