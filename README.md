# spiggy

A patcher and mod for Discord Android using [Frida](https://frida.re). Why he spin?

Frida allows us to write JavaScript instead of Java, meaning there's no compiling necessary, making it easier to write extensions than other Discord Android mods.

# Installation

## Requirements

- Linux or WSL
- [Rust](https://www.rust-lang.org/tools/install)
- [node.js](https://nodejs.org/en/download/package-manager/)
- CMake
- Make
- pkg-config
- libssl-dev

APK signing requirements:

- Java 8 or 11
- Android SDK or copies of `zipalign` and `apksigner`

## Patching the APK

```
cd HostPatcher
cargo run --bin hostpatcherlinux /path/to/discord.apk /path/to/output.apk
```

## Signing the APK

### Generating a keystore

You only need to do this step once

```
keytool -genkey -v -keystore spiggy.keystore -alias spiggy -keyalg RSA -keysize 2048 -validity 10000
```

### Align the APK

```
zipalign -p 4 /path/to/output.apk /path/to/output-aligned.apk
```

### Signing the APK

```
apksigner sign --ks-key-alias spiggy --ks /path/to/spiggy.keystore --out /path/to/output-signed.apk /path/to/output-aligned.apk
```

You can now install the APK onto your chosen device

## Building the preload

You will have to build the preload every update, as this is the file that gets loaded that contains all the extensions.

### Install packages

You only need to do this step once

```sh
# if you dont already have pnpm
npm i -g pnpm

pnpm i
```

### Build preload.js

```
./node_modules/.bin/webpack b ./preload.js
```

### Copy to device

You can do this over MTP if desired.

```sh
# if the directory isnt already created
adb shell mkdir -p /sdcard/Download/spiggy/

adb push ./dist/preload.js /sdcard/Download/spiggy/preload.js
```

# Development

This will only cover the basics of setting up an environment to develop in.

## Requirements

- Python 3 + pip
- frida-tools (`pip install frida-tools`)

## Creating a development APK

### If you are rooted and already have an APK

Go to `/data/app/<package name>/lib` and rename `libgadget.config.so` (ex: `_libgadget.config.so`)

You can rename it back to `libgadget.config.so` later if you want to revert it

### If you are not rooted

```
cargo run --bin hostpatcherlinux --preload listen /path/to/discord.apk /path/to/output.apk
```

Optionally set `-p some.other.package.name` and `-n "some other name"` if you want a seperate app.

Repeat alignment and signing steps.

## Hot-reloading preload

```
./node_modules/.bin/webpack watch ./preload.js --mode development --no-devtool
```

## Sending our preload to the app

```sh
# you only need to do this every adb session
adb forward tcp:27042 tcp:27042

# run this after you load the app
frida -R -l ./dist/preload.js -F --debug
```

You will now have your preload sent to the app everytime you make changes, along with a repl

## Decompiling Discord

[Download this fork of jadx](https://github.com/Aliucord/jadx/releases/tag/v1.2.0.82-fork1)

```
jadx -e --show-bad-code --no-debug-info --no-inline-anonymous --no-inline-methods --no-generate-kotlin-metadata --respect-bytecode-access-modifiers --fs-case-sensitive discord.apk
```

You will now have a folder with the same name as the APK with all the sources.

## Building the patcher app

Nothing super complicated. Install [`cross`](https://github.com/rust-embedded/cross):

```
cargo install cross
```

Build the image for cross-compilation:

```
podman build -t mstrodl/spiggy HostPatcher/libhostpatcher-jni
```

Build the Android app using Android Studio (or a similar tool)... Profit?
Make sure `/sdcard/Download/spiggy/BRANCH_NAME.js` exists

## Further Reading

- [Frida JavaScript API](https://frida.re/docs/javascript-api/)
- Existing extensions
- [Aliucord plugins](https://github.com/search?q=aliucord)
- [CutTheCord source](https://gitdab.com/distok/cutthecord)
